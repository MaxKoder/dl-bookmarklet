# DL-Bookmarklet

DL-Bookmarklet is a simple script to keep URL you visit and save them on a remote server.
It was originally created to keep videos urls and download them later with [youtube-dl](https://rg3.github.io/youtube-dl/).

## How it works ?

This tool consists of only 2 files: A secret file (secret-file.php), to be stored on a web server, and a small Javascript script (bookmark.js), for the client (the browser).
When you visit a site and want to save the url, just run the script, placed in a bookmarklet, and the address of the page will be stored in a previously defined file.

## Requirements

DL-Bookmarklet only require a PHP (5.4+) server (Apache or NGINX).

# Install

First, retrieve the file 'secret-file.php' and send it to your server, in a place accessible via the web.
Keep in mind the web address of this file, it will be useful later.

Then copy the contents of the 'bookmark.js' file, open your browser, create a new bookmark (or favorite), and copy the contents into it, instead of the address.

# Config

For the script to work, it must now be configured.

## Server file

First, you can rename the 'secret-file' for improve security.
And open it, there are several parameters to set up inside :

### Password

    $password = '12349';

`$password` must be a personal password to secure the exchanges and prevent potential hackers from storing links at home.
Modify it as you like and keep it in mind.

### Storage file

    $filepath = 'links.txt';

The `$filepath` must be a relative path pointing to a file.
This file will be used to store all links sent from your browser.
If the file does not exist when using DL-Bookmarklet, it will be automatically created, if permissions allow it.

### IP Filtering

    define('FILTERING_IP', true);

DL-Bookmarklet allows you to filter IPs before saving.
Set it up to `false` to disable IP filtering.

### Authorized IPs

    $authorizedIP

This var can contain many V4 IP, in the form of an array.
You can store in all the IPs you want to access the server.

Eg :

    $authorizedIP = [
        '127.0.0.1',
        '192.168.0.254',
    ];

## Bookmark File

Now let's take a look at the bookmarklet.

### Password

    var pass="12349"

In this file, in order for the server to authorize the execution of the script, it will be necessary to indicate the same password as that of the server.

Modify the `pass` variable to store the password already defined in the secret file of the server (.php).

### Secret File URL

    url="https://localhost/www/dl-bookmarklet/secret-file.php"

The last parameter is the URL that points to the server's secret file.
Insert the web address into the `url` variable.

# Issues

## Commons issues

| Issue                                | How to solve                                                                                                        |
|--------------------------------------|-------------------------------------------------------------------------------------------------------------------- |
| Permission denied for the local file | Can't create or modifiy the local file. Add permission 755 to the file or its parent, and change owner for www-data |
| Something is missing with your data  | Request was sent without password or URL. Check the JS bookmarklet.                                                 |
| Wrong password                       | Password from bookmarklet is different from password's secret file. Fix this.                                       |
| Your IP is not authorized here       | With IP filtering, your IP is not allowed to use the script. Add it into the `$authorizedIP` array.                 |
| No return detected                   | Server do not respond. Check URL in the bookmarklet.                                                                |

## HTTPS issue

Recent browsers do not support, for security reasons, to run xmlHttpRequest requests if the remote server does not have an SSL certificate.
If your server is in HTTP and not in https, you can still use DL-Bookmarklet:

Take the URL of your file, and force the https protocol.
For example, the URL of my file is `http: // max-koder.fr / secret-file.php`. In the bookmarklet, I indicate this address in https, ie `https: // max-koder.fr / secret-file.php`.

With your browser, try to access this page directly. It will suggest that you add an exception to this server that does not have a valid certificate.
And the script will now work with this browser.