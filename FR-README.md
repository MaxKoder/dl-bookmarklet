
Cet outil ne se compose que de 2 fichiers : Un fichier secret (secret-file.php), à stocker sur un serveur web, et un petit script Javascript (bookmark.js), pour le client (le navigateur).
Lorsque vous visitez un site et que vous souhaitez sauver l'url, il suffira de lancer le script, placé dans un bookmarklet, et l'adresse de la page sera stockée dans un fichier préalablement défini.

Tout d'abord, récupérez le fichier 'secret-file.php' et envoyez-le sur votre serveur, dans un endroit acessible par internet.
Gardez en mémoire l'adresse web de ce fichier, elle sera utile plus tard.

Puis copiez le contenu du fichier 'bookmark.js', ouvrez votre navigateur, créez un nouveau signet (ou favori), et copiez le contenu dans celui-ci, à la place de l'adresse.

Ouvrez le fichier 'secret-file.php'.
Il y a à l'intérieur plusieurs paramètres à configurer :

Le chemin doit être un chemin relatif pointant vers un fichier.
Ce fichier servira à stocker tous les liens envoyés depuis votre navigateur.
Si le fichier n'existe pas à l'utilisation de DL-Bookmarklet, il sera automatiquement créé, si les droits le lui permettent.

Dans ce fichier, pour que le serveur autorise l'exécution du script, il va falloir indiquer le même mot de passe que celui du serveur.

Modifiez donc la variable `pass` pour y stocker le mot de passe déjà défini dans le fichier secret du serveur (.php).

Le dernier paramètre est l'URL qui pointe vers le fichier secret du serveur.
Insérez donc l'adresse web dans la variable `url`.
