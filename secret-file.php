<?php

/**
 * Personal secure password. Must be the same as the bookmark
 */
$password = '12349';

/**
 * Relative File path for your local file
 */
$filepath = 'links.txt';

/**
 * Active filetring IP for more security
 * true :  active
 * false : inactive
 */
define('FILTERING_IP', false);

/**
 * Authorized IP array
 */
$authorizedIP = [
    '127.0.0.1',
];


/**
 * ********************** DO NOT MODIFY UNDER THIS ***********************
 */
// Send JSON response
header('content-type: application/json; charset=utf-8');
// Escape mixed-content issue
header("access-control-allow-origin: *");

checkSecurity($password, $authorizedIP);

$sendurl = $_POST['url'];

$fp = @fopen($filepath, "a+");

if ($fp === false) {
    // User is not allowed to write on the file
    sendResponse('Permission denied for the local file');
}

$result = fwrite($fp, $sendurl . PHP_EOL);
if ($result === false) {
    // User is not allowed to write on the file
    sendResponse('Permission denied for the local file');
}

fclose($fp);

sendResponse("'$sendurl' was correcty added to the file");

function sendResponse($txt) {
    echo json_encode($txt);
    exit();
}

function checkSecurity($password, $authorizedIP) {
    if (FILTERING_IP) {
        checkIP($authorizedIP);
    }
    
    if (!(isset($_POST['pass']) && isset($_POST['url']))) {
        sendResponse('Something is missing with your data');
    }

    if ($_POST['pass'] !== $password) {
        sendResponse('Wrong password');
    }
}

function checkIP($authorizedIP) {
    if (!in_array($_SERVER['REMOTE_ADDR'], $authorizedIP)) {
        sendResponse('Your IP is not authorized here');
    }
}
